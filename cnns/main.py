from tkinter import ttk, Label, Tk
from tkinter import filedialog
from PIL import Image, ImageTk

import classification

class Root(Tk):
  def __init__(self):
    super(Root, self).__init__()
    self.title("Wellcome to Python Image Classification")

    # Gets the requested values of the height and widht.
    windowWidth = self.winfo_reqwidth()
    windowHeight = self.winfo_reqheight()
    print("Width", windowWidth, "Height", windowHeight)

    positionRight = int(self.winfo_screenwidth() / 2 - windowWidth )
    positionDown = int(self.winfo_screenheight() / 2 - windowHeight*1.5 )

    # Positions the window in the center of the page.
    self.geometry("+{}+{}".format(positionRight, positionDown))
    self.minsize(400, 500)

    self.labelFrame = ttk.LabelFrame(self, text = "Choose Image")
    self.labelFrame.grid(column = 0, row = 1, padx = 20, pady = 20)

    self.button()

  def button(self):
    self.button=ttk.Button(self.labelFrame, text="Browse A File", command=self.fileDialog)
    self.button.grid(column=1, row=1)


  def fileDialog(self):
    self.filename = filedialog.asksaveasfilename(initialdir = "currdir",
                                                 title = "Select file",
                                                 filetypes=[("JPEG", "*.jpg")])
    splitName = self.filename.split('.')
    if len(splitName)>0 and splitName[len(splitName)-1] == 'jpg':
      self.filename = self.filename
    else:
      self.filename = self.filename+'.jpg'

    print(self.filename)
    if self.filename != "":
      self.label = ttk.Label(self.labelFrame, text = "")
      self.label.grid(column = 1, row = 2)
      self.label.configure(text = self.filename)

      result = classification.show_next(classification.load_image(self.filename))

      img = Image.open(self.filename)
      img = img.resize((256, 256), Image.ANTIALIAS)
      photo = ImageTk.PhotoImage(img)

      self.label2 = Label(image=photo)
      self.label2.image = photo
      self.label2.grid(column=0, row=4)

      self.lbl = ttk.Label(self, text="", font=("Helvetica", 20))
      self.lbl.grid(column=0, row=5, pady=50)
      if result == 0:
        self.lbl.configure(text='This is Cat')
      else:
        self.lbl.configure(text='This is Dog')

root = Root()
root.mainloop()