# Localization

from keras.models import load_model, Model

layer_index = -4

classifier = load_model('model_categorical_complex.h5')
weights = classifier.layers[-1].get_weights()[0]
classifier = Model(inputs=classifier.input,
                   outputs=(classifier.layers[layer_index].output, classifier.layers[-1].output))

# Load image
import numpy as np
import matplotlib.pyplot as plt


def get_heatmap(image):
  pred = classifier.predict(image)
  print("pred: ", pred)
  # conv_out = np.squeeze(conv_out)
  pred = np.argmax(pred)
  return pred

def load_image(path):
  from PIL import Image
  print(path)
  img = Image.open(path)
  img = img.resize((128, 128))
  arr = np.asarray(img)
  arr = arr / 255.0
  return arr

def show_next(image=[]):
  img = image
  img = np.expand_dims(img, axis=0)
  global out
  pred = get_heatmap(img)
  return pred